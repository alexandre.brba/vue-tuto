import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Charts from '../components/Charts.vue'
import Updates from '../components/Updates.vue'
import Team from '../components/Team.vue'

const DEFAULT_TITLE = 'BitcoinCharts';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: DEFAULT_TITLE
    }
  },
  {
    path: '/about',
    name: 'About',
    component: About,
    //component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    meta: {
      title: DEFAULT_TITLE + ' - About'
    }
  },
  {
    path : '/charts',
    name : 'Charts',
    component : Charts,
    meta: {
      title: DEFAULT_TITLE + ' - Dashboard'
    }
  },
  {
    path : '/nextUpdates',
    name : 'Updates',
    component : Updates,
    meta: {
      title: DEFAULT_TITLE + ' - Next updates'
    }
  },
  {
    path : '/Team',
    name : 'Team',
    component : Team,
    meta: {
      title: DEFAULT_TITLE + ' - Team'
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
