import firebase from 'firebase/app'
import 'firebase/firestore'

var firebaseConfig = {
    apiKey: "AIzaSyDxZrx_pMRxXL5VaIqW5NFzn8fBmwJSTwg",
    authDomain: "bitcoincharts-4df67.firebaseapp.com",
    databaseURL: "https://bitcoincharts-4df67.firebaseio.com",
    projectId: "bitcoincharts-4df67",
    storageBucket: "bitcoincharts-4df67.appspot.com",
    messagingSenderId: "510569501317",
    appId: "1:510569501317:web:1afd69334259f55ebccfec"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
export default db;
